package com.somosrenka.developers.connection;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InterfaceAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLOutput;
import java.util.LinkedHashMap;
import java.util.Map;

public class RESTConnection23
{
    public static final String HOST = "http://192.168.0.111:8000/";

    public RESTConnection23()
    {}
    public String request(String urlString)
    {
        StringBuffer chaine = new StringBuffer("");
        try
        {
            URL url = new URL(urlString);
            String token="169f319414b63822d7606770a500dd2ec71b136c";
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty("User-Agent", "MetallicaHost");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Authorization", "Token " + token);
            System.out.println(connection.getRequestProperties());
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // USANDO JSON
            JSONObject json_ = new JSONObject();
            JSONObject cred = new JSONObject();
            JSONObject auth=new JSONObject();
            JSONObject parent=new JSONObject();
            cred.put("username","adm");
            cred.put("password", "pwd");
            auth.put("tenantName", "adm");
            auth.put("passwordCredentials", cred);
            parent.put("auth", auth);

            OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());
            wr.write(parent.toString());
            Log.i("RESTConnection23", "La peticion sera : ");
            Log.i("RESTConnection23", connection.getOutputStream().toString());

            OutputStream os = connection.getOutputStream();
            os.write(parent.toString().getBytes("UTF-8"));
            Log.i("RESTConnection23", Integer.toString(connection.getResponseCode() ));
            os.close();

            Log.i("RESTConnection23", "YA NO SE QUE PEDO");
            Log.i("RESTConnection23", os.toString());
            //Log.i("RESTConnection23", os.toString());
            Log.i("RESTConnection23", "");

            //ESCRIBIENDO LOS PARAMETROS A ENVIARSE EN LA PETICION
            Map<String,Object> params = new LinkedHashMap<>();
            params.put("placas", "PlacasDelCarro");
            params.put("color", "RojoPasion");
            params.put("message", "Shark attacks in Botany Bay have gotten out of control. We need more defensive dolphins to protect the schools here, but Mayor Porpoise is too busy stuffing his snout with lobsters. He's so shellfish.");
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String,Object> param : params.entrySet())
            {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }

            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            connection.getOutputStream().write(postDataBytes);
            Log.i("RESTConnection23", "La peticion sera : ");
            Log.i("RESTConnection23", connection.getOutputStream().toString());

            //connection.connect();
            Log.i("RESTConnection23", "USANDO EL CONECTION.GETINPUTSTREAM");
            InputStream in = new BufferedInputStream(connection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder result = new StringBuilder();
            String line_;
            while ((line_= reader.readLine()) != null) {
                result.append(line_);
            }
            Log.i("RESTConnection23", "RESULTADO : ");
            Log.i("RESTConnection23", result.toString());

            Log.i("RESTConnection23", "ya se envio la peticion");
            Log.i("RESTConnection23", connection.toString());
            Log.i("RESTConnection23", "1");
            int responseCode = connection.getResponseCode();
            Log.i("RESTConnection23", connection.getResponseMessage());
            Log.i("RESTConnection23", Integer.toString(responseCode));
            if (responseCode == 200)
            {
                InputStream inputStream = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = rd.readLine()) != null)
                {
                    chaine.append(line);
                }
                System.out.println(chaine.toString());
            }
            else if (responseCode==401)
            {
                System.out.println("Autenticate por favor");
                System.out.println(connection.getErrorStream());

                InputStream inputStream = connection.getErrorStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = rd.readLine()) != null)
                {
                    chaine.append(line);
                }
                System.out.println("CHAINE401");
                System.out.println(chaine.toString());
            }
            else if (responseCode==400)
            {
                System.out.println("Error 400");
                System.out.println(connection.getErrorStream());

                InputStream inputStream = connection.getErrorStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                String line = "";
                while ((line = rd.readLine()) != null)
                {
                    chaine.append(line);
                }
                System.out.println("CHAINE400");
                System.out.println(chaine.toString());
            }

        } catch (IOException e)
        {
            // writing exception to log
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return chaine.toString();
    }

    //request 2
    public  String request_(String urlString)
    {
        StringBuffer chaine = new StringBuffer("");
        try
        {
            URL url = new URL(urlString);
            String token="169f319414b63822d7606770a500dd2ec71b136c";
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestProperty("User-Agent", "MetallicaHost");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("Authorization", "Token " + token);
            System.out.println(connection.getRequestProperties());
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // USANDO JSON
            JSONObject json_ = new JSONObject();
            json_.put("placas","misPlacas");
            json_.put("marca", "miMarca");
            json_.put("color", "miColor");

            OutputStreamWriter wr= new OutputStreamWriter(connection.getOutputStream());
            wr.write(json_.toString());
            Log.i("RESTConnection23", "La peticion sera : ");
            //Log.i("RESTConnection23", connection.getOutputStream().toString());

            OutputStream os = connection.getOutputStream();
            os.write(json_.toString().getBytes("UTF-8"));
            Log.i("RESTConnection23", Integer.toString(connection.getResponseCode() ));
            Log.i("RESTConnection23", "YA NO SE QUE PEDO");
            Log.i("RESTConnection23", os.toString());
            //Log.i("RESTConnection23", os.toString());
            Log.i("RESTConnection23", "");
            os.close();

            //intentando leer convirtiendo la respuesta a json
            // read the response
            int statusCode = connection.getResponseCode();
            if (statusCode == 400)
            {
                InputStream error_in = new BufferedInputStream(connection.getErrorStream());
                BufferedReader error_rd = new BufferedReader(new InputStreamReader(error_in));
                String line = "";
                while ((line = error_rd.readLine()) != null)
                {
                    chaine.append(line);
                }
                Log.i("RESTConnection23", "CHAINE400");
                Log.i("RESTConnection23", chaine.toString());

            }
            else if (statusCode == 200)
            {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                BufferedReader rd = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = rd.readLine()) != null)
                {
                    chaine.append(line);
                }
                Log.i("RESTConnection23", "CHAINE200");
                Log.i("RESTConnection23", chaine.toString());
            }
            else if (statusCode == 201)
            {
                InputStream in = new BufferedInputStream(connection.getInputStream());
                BufferedReader rd = new BufferedReader(new InputStreamReader(in));
                String line = "";
                while ((line = rd.readLine()) != null)
                {
                    chaine.append(line);
                }
                Log.i("RESTConnection23", "CHAINE201");
                Log.i("RESTConnection23", chaine.toString());
            }

        } catch (IOException e)
        {
            // writing exception to log
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return chaine.toString();
    }


    public static void main(String args[])
    {
        RESTConnection23 something = new RESTConnection23();
        String result = something.request(RESTConnection23.HOST+"autos/");
        System.out.println(result);
    }

}

