package com.somosrenka.developers.d_valet;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
/*
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
*/
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DatosCarro extends AppCompatActivity {
    Button siguiente;
    Button verListaCarros;
    TextView placa;
    String Splaca;
    TextView color;
    String Scolor;
    TextView modelo;
    String Smodelo;
    TextView resultado;
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                token = extras.getString("token");
                Toast.makeText(DatosCarro.this, "mostrando token en datos carro : " + token, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(DatosCarro.this, "Los extras estan vacios", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Toast.makeText(DatosCarro.this, "No se encontro el token", Toast.LENGTH_SHORT).show();
        }
        setContentView(R.layout.activity_datos_carro);
        siguiente = (Button) findViewById(R.id.CarroBtnGuardar);
        verListaCarros = (Button) findViewById(R.id.taco);
        verListaCarros.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                verListaCarros.setEnabled(false);
                //REDIRECCIONANDO AL ACTIVITY PARA VER LA LISTA DE AUTOS
                Log.i("DatosCarro","Click al boton DatosCarro");
                Intent redireccion = new Intent(DatosCarro.this, ListaAutos.class);
                redireccion.putExtra("token", token);
                startActivity(redireccion);
            }
        }
        );
        try
        {
            placa = (TextView) findViewById(R.id.CarroTxtPlacas);
            Splaca = placa.getText().toString();
            color = (TextView) findViewById(R.id.CarroTxtColor);
            Scolor = color.getText().toString();
            modelo = (TextView) findViewById(R.id.CarroTxtModelo);
            Smodelo = modelo.getText().toString();
            resultado = (TextView) findViewById(R.id.CarroTxtResultado);
            siguiente.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(placa.getText())) {
                        Toast.makeText(DatosCarro.this, "Falta la placa", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(color.getText())) {
                        Toast.makeText(DatosCarro.this, "Falta el color", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(modelo.getText())) {
                        Toast.makeText(DatosCarro.this, "Falta el modelo", Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(token)) {
                        Toast.makeText(DatosCarro.this, "Se perdio el token por favor identifiquese", Toast.LENGTH_SHORT).show();
                    } else {
                        /*
                        if (android.os.Build.VERSION.SDK_INT > 9) {
                            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                            StrictMode.setThreadPolicy(policy);
                        }
                        String direccion = "http://192.168.0.111:8000/autos/";
                        HttpClient client = new DefaultHttpClient();
                        HttpPost post = new HttpPost(direccion);
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("placas", placa.getText().toString()));
                        params.add(new BasicNameValuePair("color", color.getText().toString()));
                        params.add(new BasicNameValuePair("marca", modelo.getText().toString()));
                        post.setHeader("Authorization", "Token " + token);
                        try {
                            post.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                            HttpResponse httpResponse = client.execute(post);
                            InputStream inputStream = httpResponse.getEntity().getContent();
                            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                            StringBuilder stringBuilder = new StringBuilder();
                            String bufferedStrChunk = null;

                            while ((bufferedStrChunk = bufferedReader.readLine()) != null)
                            {
                                stringBuilder.append(bufferedStrChunk);
                            }
                            resultado.setText(stringBuilder);

                            //REDIRECCIONANDO AL ACTIVITY PARA VER LA LISTA DE AUTOS
                            Intent redireccion = new Intent(DatosCarro.this, ListaAutos.class);
                            redireccion.putExtra("token", token);
                            startActivity(redireccion);

                        } catch (Exception e) {
                            Toast.makeText(DatosCarro.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                        */
                    }
                }
            });
        } catch (Exception e) {
            Toast.makeText(DatosCarro.this, e.toString(), Toast.LENGTH_SHORT).show();
        }

    }
}
