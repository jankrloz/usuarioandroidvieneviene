package com.somosrenka.developers.d_valet;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.somosrenka.developers.connection.RESTConnection23;
import com.somosrenka.developers.functions.HttpRESTConnection;
import com.somosrenka.developers.functions.JsonAutos;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.io.IOException;

public class ListaAutos extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        String token="";

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_autos);

        //Aqui ya puedo hacer mi desmadre
        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                token = extras.getString("token");
                Toast.makeText(ListaAutos.this, "mostrando token en ListaAutos : " + token, Toast.LENGTH_SHORT).show();
                Log.i("ListaAutos", "mostrando token en datos carro : " + token);
            } else {
                Toast.makeText(ListaAutos.this, "Los extras estan vacios", Toast.LENGTH_SHORT).show();
                Log.i("ListaAutos", "Los extras estan vacios");
            }
        } catch (Exception e) {
            Toast.makeText(ListaAutos.this, "No se encontro el token", Toast.LENGTH_SHORT).show();
            Log.i("ListaAutos", "No se encontro el token");
        }

        ////////////Conectando con el servidor para obtener la lista de carros del usuario
        //Creando clase para conectar al servidor
        String host = "http://192.168.0.111:8000/autos/";
        String jsonString = null;

        //preparando la coneccion, agregando headers y token antes de enviar la peticion
        //  CODIGO DEL SDK22
        /*
            HttpRESTConnection connection = new HttpRESTConnection(host);
            connection.setParameter("token", token);
            connection.setHeader("Authorization", "Token " + token);
            try
            {
                //Conectamos
                jsonString = connection.AutosListConnection(host);
                //TENEMOS EL PARSE DE LA RESPUESTA A FORMATO JSON
                JsonAutos json_login = new JsonAutos(jsonString);
                //VERIFICAMOS QUE LA PETICION HALLA SIDO EXITOSA
                int status_ = connection.getStatusCode();
                Log.i("ListaAutos","StatusCOde es "+Integer.toString(status_));
                if (status_ == 200)
                {
                    //SI EL STATUS ES 201 ES QUE FUE EXITOSA LA PETICION
                    Log.i("ListaAutos","Fue exitosa la peticion");
                }
                else
                {
                    Log.i("ListaAutos","Error en la peticion, STATUS: "+Integer.toString(status_));
                }

            } catch (IOException e)
            {
                Log.i("ListaAutos","Error de IOExceptionxeption");
                Log.i("ListaAutos",e.toString());
            } catch (JSONException e)
            {
                Log.i("ListaAutos","Error de JSONException");
                Log.i("ListaAutos",e.toString());
            }
        */

        //PARA LA VERSION 23
        RESTConnection23 something = new RESTConnection23();
        Log.i("ListaAutos", "ANTES DE ENVIAR LA PETICION DE AUTOS");
        String result = something.request_(RESTConnection23.HOST+"autos/");
        Log.i("ListaAutos", result);



    }
}
