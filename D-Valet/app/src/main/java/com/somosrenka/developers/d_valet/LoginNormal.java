package com.somosrenka.developers.d_valet;


import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.somosrenka.developers.functions.JsonLogin;
import com.somosrenka.developers.functions.JsonRegistro;
/*
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;
*/
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class LoginNormal extends AppCompatActivity {
    Button siguiente;
    EditText email;
    EditText pass1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_normal);

        try {
            siguiente = (Button) findViewById(R.id.LoginIdentificarseBtn);
            siguiente.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    email = (EditText) findViewById(R.id.LoginEmailTxt);
                    pass1 = (EditText) findViewById(R.id.LoginPassTxt);
                    TextView textView = (TextView) findViewById(R.id.LoginResultadoTxt);
                    if (TextUtils.isEmpty(email.getText().toString())) {
                        textView.setText("Falta el mail");
                    } else if (TextUtils.isEmpty(pass1.getText().toString())) {
                        textView.setText("Falta el password");
                    } else {
                        try {
                            if (android.os.Build.VERSION.SDK_INT > 9) {
                                StrictMode.ThreadPolicy policy =
                                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                StrictMode.setThreadPolicy(policy);
                            }
                            /*
                            String direccion = "http://192.168.0.111:8000/rest-auth/login/";
                            HttpClient client = new DefaultHttpClient();
                            HttpPost post = new HttpPost(direccion);
                            List<NameValuePair> params = new ArrayList<NameValuePair>();
                            params.add(new BasicNameValuePair("username", email.getText().toString()));
                            params.add(new BasicNameValuePair("password", pass1.getText().toString()));
                            params.add(new BasicNameValuePair("email", email.getText().toString()));
                            post.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                            HttpResponse httpResponse = client.execute(post);
                            InputStream inputStream = httpResponse.getEntity().getContent();

                            ///
                            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                            StringBuilder response = new StringBuilder();
                            String bufferedStrChunk = null;
                            //Guardando respuesta (JSON) del servidor
                            while ((bufferedStrChunk = bufferedReader.readLine()) != null)
                            { response.append(bufferedStrChunk); }
                            //Guardando el token del usuario
                            JsonLogin json_login = new JsonLogin(response.toString());
                            JSONObject jObject = new JSONObject(response.toString());
                            if (json_login.has_token())
                            {
                                String token = jObject.getString("key");
                                Toast.makeText(LoginNormal.this, "Mostrando token recibido : " + token, Toast.LENGTH_SHORT).show();
                                Intent redireccion = new Intent(LoginNormal.this, DatosCarro.class);
                                redireccion.putExtra("token", token);
                                startActivity(redireccion);
                            }
                            else
                            {
                                Toast.makeText(LoginNormal.this, json_login.getError(), Toast.LENGTH_SHORT).show();
                            }
                            textView.setText(response);
                            */
                        } catch (Exception e) {
                            //e.printStackTrace();
                            textView.setText(e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                        }

                    }
                }
            });
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
