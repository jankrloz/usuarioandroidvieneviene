package com.somosrenka.developers.d_valet;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button Identificarme;
    Button Registrarme;
    Button Mapa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            Identificarme = (Button) findViewById(R.id.BtnRegistros);
            Registrarme = (Button) findViewById(R.id.BtnLogin);
            Mapa = (Button) findViewById(R.id.btnMapa);

            Identificarme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, LogIn.class));
                }
            });

            Registrarme.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, Registro.class));
                }
            });
            Mapa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, Ubicacion.class));
                }
            });
        } catch (Exception e) {
            System.out.println(e.getCause());
            System.out.println(e.getMessage());
            System.out.println(e.getClass());
        }

    }

}
