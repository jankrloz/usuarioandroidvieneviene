package com.somosrenka.developers.d_valet;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.somosrenka.developers.functions.JsonRegistro;
import com.somosrenka.developers.models.Usuario;
/*
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
*/
import org.json.JSONObject;
//import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class NuevoUsuario extends AppCompatActivity {
    EditText email;
    EditText pass1;
    EditText pass2;
    TextView resultado;
    String servidor;
    Button btnRegistrar;
    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_usuario);

        try {
            btnRegistrar = (Button) findViewById(R.id.BtnResgistro);
            resultado = (TextView) findViewById(R.id.RegistroTxtResultado);
            btnRegistrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    email = (EditText) findViewById(R.id.valueEmail);
                    pass1 = (EditText) findViewById(R.id.valuePass1);
                    pass2 = (EditText) findViewById(R.id.valuePass2);
                    servidor = (R.string.SERVIDOR) + "";
                    TextView textView = (TextView) findViewById(R.id.RegistroTxtResultado);
                    if (TextUtils.isEmpty(email.getText().toString())) {
                        textView.setText("Falta el mail");
                    } else if (TextUtils.isEmpty(pass2.getText().toString())) {
                        textView.setText("Falta el password 2");
                    } else if (TextUtils.isEmpty(pass1.getText().toString())) {
                        textView.setText("Falta el password 1");
                    } else if (pass1.getText().equals(pass2.getText().toString())) {
                        textView.setText("El password no empata");
                    } else {
                        try {
                            if (android.os.Build.VERSION.SDK_INT > 9) {
                                StrictMode.ThreadPolicy policy =
                                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                                StrictMode.setThreadPolicy(policy);
                            }
                            /*
                            String direccion = "http://192.168.0.111:8000/rest-auth/registration/";
                            HttpClient client = new DefaultHttpClient();
                            HttpPost post = new HttpPost(direccion);
                            List<NameValuePair> params = new ArrayList<NameValuePair>();
                            params.add(new BasicNameValuePair("username", email.getText().toString()));
                            params.add(new BasicNameValuePair("password1", pass1.getText().toString()));
                            params.add(new BasicNameValuePair("password2", pass1.getText().toString()));
                            params.add(new BasicNameValuePair("email", email.getText().toString()));
                            post.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
                            HttpResponse httpResponse = client.execute(post);
                            InputStream inputStream = httpResponse.getEntity().getContent();

                            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                            //stringBuilder es donde se guarda la respuesta en formato JSON
                            StringBuilder respuesta_ = new StringBuilder();
                            String bufferedStrChunk = null;
                            while ((bufferedStrChunk = bufferedReader.readLine()) != null)
                            { respuesta_.append(bufferedStrChunk); }
                            textView.setText(respuesta_);

                            /////////////// guardar en un OBJETO USUARIO los datos despues de obtener la llave
                            JsonRegistro json = new JsonRegistro(respuesta_.toString());
                            if(json.has_token())
                            {
                                //Log.i("NuevoUsuario", "jObject vale " + jObject.toString());
                                //Log.i("NuevoUsuario", "Valor de token es" + jObject.getString("key"));
                                String token = json.getToken();
                                usuario = Usuario.getInstance(email.getText().toString(), token);
                                //Log.i("NuevoUsuario", "Imprimiendo usuario");
                                //Log.i("NuevoUsuario", usuario.toString());
                                Intent redireccion=new Intent(NuevoUsuario.this, DatosCarro.class);
                                redireccion.putExtra("token",token);
                                startActivity(redireccion);
                                Toast.makeText(getApplicationContext(), "Bienvenido " +usuario.getEmail(), Toast.LENGTH_LONG).show();
                                //Log.i("NuevoUsuario", "La respuesta fue " + stringBuilder);
                            }
                            else
                            {
                                //error en el logueo
                                resultado.setText(json.getErrors().toString());
                                Log.i("NuevoUsuario", "La respuesta fue " + json.getErrors().toString());
                            }
                            */
                        } catch (Exception e) {
                            e.printStackTrace();
                            textView.setText(e.toString());
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                            Log.i("NuevoUsuario", "Error : " + e.toString());
                        }
                        //String url = "http://192.168.0.111:8000/rest-auth/registration/?username=" + email.getText().toString() + "&email=" + email.getText().toString() + "&password1=" + pass1.getText().toString() + "&password2=" + pass2.getText().toString();
                        //new EnviarRegistroNuevo().execute(url);
                        //textView.setText(url);
                    }
                }
            });
        } catch (Exception e) {
            Log.i("NuevoUsuario", e.getMessage());
            Log.i("NuevoUsuario", e.getCause().toString());
            Log.i("NuevoUsuario", e.getClass().toString());
        }
    }
    /*
    private class EnviarRegistroNuevo extends AsyncTask<String, Long, String> {

        protected String doInBackground(String... urls) {
            try {
                return HttpRequest.post(urls[0]).accept("application/json").body();
            } catch (HttpRequest.HttpRequestException exception) {
                return null;
            }
        }

        protected void onPostExecute(String response) {
            TextView textView = (TextView) findViewById(R.id.ResultadoRegistro);
            if (!response.isEmpty()) {
                textView.setText(response.toString());
            }

        }
    }
    */
}
