package com.somosrenka.developers.functions;

/*
Clase para manejar las peticiones al servidor

*/

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class HttpRESTConnection
{
    private String host;
    private HttpClient client;
    private HttpPost post;
    private HttpResponse httpResponse;
    private List<NameValuePair> parameters=new ArrayList<NameValuePair>();

    public HttpRESTConnection()
    {
        this.client = new DefaultHttpClient();
        this.post = new HttpPost(host);
    }

    public HttpRESTConnection(String host)
    {
        this.host=host;
        this.client = new DefaultHttpClient();
        this.post = new HttpPost(host);
    }

    //Metodo para Decodificar la peticion realizada al servidor, y pasarla a un String
    private String decodeRequest() throws IOException
    {
        //Clases para leer el contenido de la peticion
        InputStream inputStream = this.httpResponse.getEntity().getContent();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        //variable para guardar el resultado
        StringBuilder response = new StringBuilder();
        String bufferedStrChunk = null;
        //Guardando respuesta (JSON) del servidor
        while ((bufferedStrChunk = bufferedReader.readLine()) != null)
        { response.append(bufferedStrChunk); }
        return response.toString();
    }

    //Metodo raiz/generico para conectar con el servidor
    public String GenericConnection(String host, List<NameValuePair> paramms) throws IOException
    {
        //Inicializando el host a donde hacer la peticion e inicializando las clases para hacer la peticion
        this.host=host;
        //Agregando parametros
        post.setEntity(new UrlEncodedFormEntity(paramms, HTTP.UTF_8));
        this.httpResponse = this.client.execute(post);
        return decodeRequest();
    }


    public String LoginConnection(String host) throws IOException
    {
        //Inicializando el host a donde hacer la peticion e inicializando las clases para hacer la peticion
        this.host=host;
        //Agregando parametros
        post.setEntity(new UrlEncodedFormEntity(this.parameters, HTTP.UTF_8));
        this.httpResponse = this.client.execute(post);
        return decodeRequest();
    }

    public String AutosListConnection(String host) throws IOException
    {
        //Inicializando el host a donde hacer la peticion e inicializando las clases para hacer la peticion
        this.host=host;
        //Agregando parametros
        post.setEntity(new UrlEncodedFormEntity(this.parameters, HTTP.UTF_8));
        this.httpResponse = this.client.execute(post);
        return decodeRequest();
    }


    public void setParameter(String name, String value)
    {
        this.parameters.add(new BasicNameValuePair(name, value));
    }

    public void setHeader(String name, String value )
    {
        this.post.setHeader(name,value);
    }

    public int getStatusCode()
    {
        return this.httpResponse.getStatusLine().getStatusCode();
    }



    /*

    String direccion = "http://192.168.0.111:8000/rest-auth/login/";
    HttpClient client = new DefaultHttpClient();
    HttpPost post = new HttpPost(direccion);
    List<NameValuePair> params = new ArrayList<NameValuePair>();

    params.add(new BasicNameValuePair("username", email.getText().toString()));
    params.add(new BasicNameValuePair("password", pass1.getText().toString()));
    params.add(new BasicNameValuePair("email", email.getText().toString()));
    post.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
    HttpResponse httpResponse = client.execute(post);
    InputStream inputStream = httpResponse.getEntity().getContent();

    ///
    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
    StringBuilder response = new StringBuilder();
    String bufferedStrChunk = null;
    //Guardando respuesta (JSON) del servidor
    while ((bufferedStrChunk = bufferedReader.readLine()) != null)
    { response.append(bufferedStrChunk); }
    */




}
