package com.somosrenka.developers.functions;

import com.somosrenka.developers.models.Auto;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

/*
*CLASE PARA MANEJAR EL JSON DE RESPUESTA AL PEDIR LA LISTA DE AUTOS
*/
public class JsonAutos
{
    public JSONObject jObject;

    public JsonAutos(String json) throws JSONException
    { jObject = new JSONObject(json); }

    public List<Auto> getAutos() throws JSONException
    {
        ArrayList<Auto> autosList = new ArrayList<>();
        String placa="";
        String marca="";
        String color="";

        placa=jObject.optString("placas");
        color=jObject.optString("color");
        marca=jObject.optString("marca");
        autosList.add(new Auto(placa, color, marca));
        return autosList;
    }

    public List<String> getErrors() throws JSONException
    {
        ArrayList<String> array = new ArrayList<>();
        array.add(jObject.optString("placas"));
        array.add(jObject.optString("color"));
        array.add(jObject.optString("marca"));
        return array;
    }
}
