package com.somosrenka.developers.functions;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonLogin
{
    public JSONObject jObject;

    public JsonLogin(String json) throws JSONException
    { jObject = new JSONObject(json); }

    public boolean has_token() throws JSONException
    { return jObject.has("key"); }

    public String getToken() throws JSONException
    { return jObject.getString("key"); }

    public String getError() throws JSONException
    { return jObject.optString("detail"); }

}
