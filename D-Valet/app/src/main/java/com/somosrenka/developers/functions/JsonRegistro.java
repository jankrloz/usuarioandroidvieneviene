package com.somosrenka.developers.functions;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class JsonRegistro
{
    public JSONObject jObject;

    public JsonRegistro(String json) throws JSONException
    { jObject = new JSONObject(json); }

    public boolean has_token() throws JSONException
    { return jObject.has("key"); }

    public String getToken() throws JSONException
    { return jObject.getString("key"); }

    public List<String> getErrors() throws JSONException
    {
        ArrayList<String> array = new ArrayList<>();
        array.add(jObject.optString("email"));
        array.add(jObject.optString("username"));
        array.add(jObject.optString("password1"));
        return array;
    }
}
