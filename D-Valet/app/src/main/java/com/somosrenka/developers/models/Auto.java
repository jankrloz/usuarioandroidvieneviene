package com.somosrenka.developers.models;

public class Auto
{
    private int id;
    private String placa;
    private String color;
    private String marca;

    public Auto(String placa, String color, String marca)
    {
        this.placa = placa;
        this.color = color;
        this.marca = marca;
    }

    public int getId()
        { return id; }

    public void setId(int id)
        { this.id = id; }

    public String getPlaca()
        { return placa; }

    public void setPlaca(String placa)
        { this.placa = placa; }

    public String getColor()
        { return color; }

    public void setColor(String color)
        {this.color = color; }

    public String getMarca()
        {return marca; }

    public void setMarca(String marca)
        { this.marca = marca; }
}
