package com.somosrenka.developers.models;

public class Usuario
{
    private String token;
    private String email;
    private static Usuario miUsuario;

    private Usuario(String email, String token)
    {
        this.email=email;
        this.token=token;
    }

    public  static Usuario getInstance(String email, String token)
    {
        if (miUsuario==null)
            { miUsuario=new Usuario(email, token); }
        return miUsuario;
    }

    public static void setInstance(String email, String token)
    { miUsuario = new Usuario(email, token); }

    public String getToken()
    { return token; }

    public void setToken(String token)
    { this.token = token; }

    public String getEmail()
    { return email; }

    public void setEmail(String email)
    { this.email = email; }

    public String toString()
    { return this.email + " - " + this.token; }

}
